from storage.storage import DataStorage
from storage.data_base_exception import Data_base_Exception
from sqlalchemy.orm import relationship
from storage.sql_alchemy import db
import logging

logger = logging.getLogger()

"""
One To Many: https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html#one-to-many
with bidirectional relationship in one-to-many where the 'reverse' side is a many to one
"""
class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(100), nullable=False)
    registration_date = db.Column(db.Date, nullable=False)
    eventsFromUser = db.relationship('Events', back_populates="user")

class Events(db.Model):
    __tablename__ = 'events'
    id = db.Column(db.Integer, primary_key=True)
    userId =  db.Column(db.Integer, db.ForeignKey('users.id'))
    event_type = db.Column(db.String(30), nullable=False)
    event_datetime =  db.Column(db.DateTime, nullable=False)
    user = relationship("Users", back_populates="eventsFromUser")


class MySQLStorage(DataStorage):
    

    def __init__(self):
        self.db = db
    
    def get_db(self):
        return self.db

    def set_pagination(self, pagination=None):
        self.pagination = pagination
    
    def set_logger(self, logger=None):
        self.logger = logger

    def insert_user(self, data):
        try:
            # Create new user
            user = Users(nickname=data["nickname"],
                    registration_date=data["registration_date"])
            self.db.session.add(user)
            self.db.session.commit()
        except Exception as err:
            self.logger.error('failed to include in database: %s', err)
            raise Data_base_Exception ({"code": 500, "_type": "error", "message": "failed to include in database"})

    def get_user(self, nickname:str):
        users = []
        try:
            if nickname:
                users = Users.query.filter(
                    Users.nickname.startswith(nickname)).all()
            else:
                users = Users.query.order_by(Users.nickname).all()
        except Exception as err:
            self.logger.error('failed to retrive from the database: %s', err)
            raise Data_base_Exception ({"code": 500, "_type": "error", "message": "failed to comunicate with the database"})
        return users


    def update_user(self, data, userId):
        # verify if user id already exists, otherwise abort
        try:
            # Create user with informed parameters
            user = Users(id=userId, nickname=data["nickname"],
                    registration_date=data["registration_date"])
            exists = Users.query.filter(Users.id == user.id).first()
            if not exists:
                raise Exception ({"code": 404, "_type": "error", "message": "User not found with the informed Id"})
        except Exception as err:
            self.logger.error('failed to retrive from the database: %s', err)
            raise Data_base_Exception ({"code": 500, "_type": "error", "message": "failed to comunicate with the database"})

        # update database
        try:
            # Examines the primary key attributes of the source instance, and attempts to reconcile it
            # with an instance of the same primary key in the session
            self.db.session.merge(user)
            self.db.session.commit()
        except Exception as err:
            self.logger.error('failed to update user in database: %s', err)
            raise Data_base_Exception ({"code": 500, "_type": "error", "message": "failed to comunicate with the database"})
        
        

    def get_events(self, userId, startDate: int, endDate: int, sort, page: int):
        try:
            if not Users.query.filter(Users.id == userId).first():
                return make_response(jsonify(api_resp.dump(
                    {"code": 404, "_type": "error", "message": "User not found"})), 404)
        
            query = Events.query.filter(Events.userId == userId)

            if startDate and endDate:
                query = Events.query.filter(Events.event_datetime >= startDate, Events.event_datetime <= endDate)
            elif startDate:
                query = Events.query.filter(
                    Events.event_datetime >= startDate)
            elif endDate:
                query = Events.query.filter(
                    Events.event_datetime <= endDate)

            if  sort and sort == "desc":
                query = query.order_by(Events.event_datetime.desc())
            elif sort:
                query = query.order_by(Events.event_datetime.asc())

            if not page:
                page = 0

            events = query.paginate(page=page, error_out=False, max_per_page=3)

        except Exception as err:
            self.logger.error('failed to comunicate with the database: %s', err)
            raise Data_base_Exception({"code": 500, "_type": "error", "message": "failed to comunicate with the database"})
        return events
    
    def get_specific_event_user(self, eventId):
        try:
            events = Events.query.filter(Events.id == eventId).first()
        except Exception as err:
            self.logger.error('failed to retribe from the database: %s', err)
            raise Data_base_Exception({"code": 500, "_type": "error", "message": "failed to comunicate with the database"})
    
        return events
