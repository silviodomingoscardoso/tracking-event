import logging
from os import environ
from datetime import datetime, timedelta
from flask import jsonify, request, Blueprint, make_response, url_for
from kombu import Connection, Exchange, Queue
from model.models import EventSchema, UserSchema, UserPostSchema, UserPutSchema, EventPostSchema, ApiResponse
from marshmallow import ValidationError
from storage.util_db import db
from messaging.celery_conf import celery
from storage.data_base_exception import Data_base_Exception

logger = logging.getLogger()

user_post = UserPostSchema()
user_update = UserPutSchema()
users_schema = UserSchema(many=True)
events_schema = EventSchema(many=True)
event_schema = EventSchema(many=False)
event_post = EventPostSchema()
api_resp = ApiResponse()


event_api = Blueprint('tracking', __name__, url_prefix='/v2')


def get_blueprint():
    """Return the blueprint for the main app module"""
    return event_api


@event_api.route("/user", methods=['POST'])
def createUser():
    if not request.get_json():
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "body not present"})), 400)

    data = request.get_json(force=True)

    try:
        data = user_post.load(data)
    except ValidationError as err:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": str(err.messages)})), 400)

    try:
        db.insert_user(data)
    except Data_base_Exception as err:
        return make_response(jsonify(err.json), err.json["code"])
        
    return make_response(jsonify(api_resp.dump(
        {"code": 201, "_type": "sucess", "message": "User created"})), 201)


@event_api.route("/user", methods=['GET'])
def getUserByName():
    try:
        users = db.get_user(request.args.get("nickname"))
    except Data_base_Exception as err:
        return make_response(jsonify(err.json), err.json["code"])

    try:
        result = users_schema.dump(users)
    except ValidationError as err:
        return make_response(jsonify(api_resp.dump(
            {"code": 500, "_type": "error", "message": "database doesnt match API specification"})), 500)

    return make_response(jsonify(result), 200)


@event_api.route("/user/<int:userId>", methods=['PUT'])
def updateUser(userId):
    if not userId:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "parameter userId is required"})), 400)

    if not request.get_json():
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "missing body"})), 400)

    data = request.get_json(force=True)
    data["id"] = userId

    try:
        data = user_update.load(data)
    except ValidationError as err:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": str(err.messages)})), 400)

    try:
        db.update_user(data, userId)
    except Data_base_Exception as err:
        return make_response(jsonify(err.json), err.json["code"])

    return make_response(jsonify(api_resp.dump(
        {"code": 200, "_type": "sucess", "message": "User updated"})), 200)


@event_api.route("/events", methods=['GET'])
def listEvents():
    if not request.args.get('userId'):
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "parameter userId is required"})), 400)
    userId = int(request.args.get('userId'))

    if userId <= 0:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "User id must be greater than 0"})), 400)

    if not request.args.get('page'):
        page = 1
    elif int(request.args.get('page')) <= 0:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "page must be greater than 0"})), 400)
    else:
        page = int(request.args.get('page'))
    try:
        events = db.get_events(userId, request.args.get('startDate'), request.args.get('endDate'),
                               request.args.get('sort'), page)
    except Data_base_Exception as err:
        return make_response(jsonify(err.json), err.json["code"])
    # Serialize the queryset
    try:
        prev_page = None
        if events.has_prev:
            prev_page = url_for('.listEvents',
                                startDate=request.args.get('startDate'),
                                endDate=request.args.get('endDate'),
                                sort=request.args.get('sort'),
                                page=events.prev_num,
                                _external=False)
        next_page = None
        if events.has_next:
            next_page = url_for('.listEvents',
                                startDate=request.args.get('startDate'),
                                endDate=request.args.get('endDate'),
                                sort=request.args.get('sort'),
                                page=events.next_num,
                                _external=False)

        result = {"events": events_schema.dump(
            events.items), "next": next_page, "prev": prev_page}

    except ValidationError as err:
        return make_response(jsonify(api_resp.dump({"code": 500, "_type": "error",
                                                    "message": "database doesnt match API specification"})), 500)

    return make_response(jsonify(result), 200)


@event_api.route("/events", methods=['POST'])
def postEvent():
    if not request.get_json():
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "body not present"})), 400)

    data = request.get_json(force=True)

    try:
        data = event_post.load(data)
    except ValidationError as err:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": str(err.messages)})), 400)

    try:
        celery.send_task('app.app.store_event',args=[request.get_json()],  exchange='events', routing_key='tasks.store_event')
    except Exception as err:
        logger.error(err)
        return make_response(jsonify(api_resp.dump(
            {"code": 500, "_type": "error", "message": "Error while publishing event, check your message broker!"})), 500)

    return make_response(jsonify(api_resp.dump(
        {"code": 201, "_type": "success", "message": "Event published!" })), 201)


@event_api.route("/events/<int:eventId>", methods=['GET'])
def specificEvent(eventId):
    if not eventId:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "Event id is requeired"})), 400)
    if eventId <= 0:
        return make_response(jsonify(api_resp.dump(
            {"code": 400, "_type": "error", "message": "Event id must be greater than 0"})), 400)

    # call db
    try:
        events = db.get_specific_event_user(eventId)
    except Data_base_Exception as err:
        return make_response(jsonify(err.json), err.json["code"])
    try:
        result = event_schema.dump(events)
    except ValidationError as err:
        return make_response(jsonify(
            api_resp.dump({"code": 500,
                           "_type": "error",
                           "message": "database doesnt match API specification"})), 500)

    return make_response(jsonify(result), 200)
