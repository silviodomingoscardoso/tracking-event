from os import environ

class DevelopmentConfig(object):
    SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://{environ.get('DB_USER')}:{environ.get('DB_PASSWORD')}@{environ.get('DB_URL')}:{environ.get('DB_PORT')}/{environ.get('DB_SCHEMA')}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
