from sqlalchemy.orm import relationship
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(100), nullable=False)
    registration_date = db.Column(db.Date, nullable=False)
    eventsFromUser = db.relationship('Events', back_populates="user")

class Events(db.Model):
    __tablename__ = 'events'
    id = db.Column(db.Integer, primary_key=True)
    userId =  db.Column(db.Integer, db.ForeignKey('users.id'))
    event_type = db.Column(db.String(30), nullable=False)
    event_datetime =  db.Column(db.DateTime, nullable=False)
    user = relationship("Users", back_populates="eventsFromUser")