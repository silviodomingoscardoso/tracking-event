import requests
import pytest
import json
from os import environ
from dateutil.parser import parse

BASE_URL = environ.get("BASE_URL", "http://localhost/v2")


def test_create_event_201():

    url = f"{BASE_URL}/events"
    payload = "{\r\n  \"userId\": 2,\r\n  \"event_type\": \"tester\",\r\n  \"event_datetime\": \"2017-07-21T17:32:28Z\"\r\n}"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 201

def test_create_1000_events():
    for i in range(0,1000):
        test_create_event_201()

def test_create_event_400_missing_userId():

    url = f"{BASE_URL}/events"
    payload = "{\r\n  \"event_type\": \"tester\",\r\n  \"event_datetime\": \"2017-07-21T17:32:28Z\"\r\n}"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_create_event_400_missing_event_type():

    url = f"{BASE_URL}/events"
    payload = "{\r\n  \"userId\": 2,\r\n  \"event_datetime\": \"2017-07-21T17:32:28Z\"\r\n}"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_create_event_400_missing_event_datetime():

    url = f"{BASE_URL}/events"
    payload = "{\r\n  \"userId\": 2,\r\n  \"event_type\": \"tester\" \r\n}"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_create_event_400_date_future():

    url = f"{BASE_URL}/events"
    payload = "{\r\n  \"userId\": 2,\r\n  \"event_type\": \"tester\",\r\n  \"event_datetime\": \"3017-07-21T17:32:28Z\"\r\n}"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_create_event_403():

    url = f"{BASE_URL}/events"
    payload = "{\r\n  \"userId\": 2,\r\n  \"event_type\": \"tester\",\r\n  \"event_datetime\": \"2017-07-21T17:32:28Z\"\r\n}"
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 403


def test_get_event_200():

    url = f"{BASE_URL}/events?userId=2"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 200
    assert len(response.json()) > 1


def test_get_user_event_400_missing_userId():

    url = f"{BASE_URL}/events"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_get_user_event_400_missing_userId_contain_other_params():

    url = f"{BASE_URL}/events?sort=asc&endDate=2020-12-31"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 400

def test_get_event_400_zero_userId():

    url = f"{BASE_URL}/events/0"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 400

def test_get_event_403_1():

    url = f"{BASE_URL}/events?userId=2"

    payload = {}
    headers = {
        'Accept': 'application/json',
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 403


def test_get_event_403_2():

    url = f"{BASE_URL}/events/2"

    payload = {}
    headers = {
        'Accept': 'application/json',
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 403


def test_get_event_user_2_date_greater_2020():

    url = f"{BASE_URL}/events?userId=2&sort=asc&startDate=2019-12-31"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 200
    for item in response.json()["events"]:
        assert parse(item["event_datetime"]) >= parse("2020-01-01 00:00:00")


def test_get_event_user_2_date_lower_2020():

    url = f"{BASE_URL}/events?userId=2&sort=desc&endDate=2020-12-31"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 200
    for item in response.json()["events"]:
        assert parse(item["event_datetime"]) <= parse("2020-12-31 23:59:59")


def test_get_event_user_2_date_between_2020_2021():

    url = f"{BASE_URL}/events?userId=2&sort=asc&startDate=2020-01-01&endDate=2020-12-31"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 200
    for item in response.json()["events"]:
        assert parse(item["event_datetime"]) >= parse("2020-01-01 00:00:00") and parse(item["event_datetime"]) <= parse("2020-12-31 23:59:59")


def test_get_event_user_2_sort_asc():

    url = f"{BASE_URL}/events?userId=2&sort=asc"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 200
    assert len(response.json()["events"]) > 1
    data = response.json()["events"]
    assert parse(data[0]["event_datetime"]) <= parse(data[-1]["event_datetime"])


def test_get_event_user_2_sort_desc():

    url = f"{BASE_URL}/events?userId=2&sort=desc"

    payload = {}
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme'
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    assert response.status_code == 200
    assert len(response.json()["events"]) > 1
    data = response.json()["events"]
    assert parse(data[0]["event_datetime"]) > parse(data[-1]["event_datetime"])
