from abc import ABC

class DataStorage(ABC):

    def insert_user(self, data):
        pass

    def get_user(self, nickname:str):
        pass

    def update_user(self, data, userId):
        pass

    def get_events(self, userId, startDate, endDate, order):
        pass
    
    def get_specific_user(self, userId):
        pass

    def get_db():
        pass

    def set_pagination(pagination):
        pass

    def set_logger(logger):
        pass

