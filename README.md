# Tracking events

### Running

1. Setup the configurations for elasticsearch on: https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_set_vm_max_map_count_to_at_least_262144

Look at your system to properly setup vm.max_map_count

Easy for copy + paste


Windows:
 ```
wsl -d docker-desktop
sysctl -w vm.max_map_count=262144
```

macOs with docker desktop
```
docker-machine ssh
sudo sysctl -w vm.max_map_count=262144
```



2. run `docker-compose up -d` in the project root

## Access the apps

  *  Flask API => http://localhost/api/docs/ `apikey: changeme`
  *  Kibana => http://localhost:5601
  *  rabbitmq => http://localhost:15672 `user: admin  password: changeme`
  *  phpMyAdmin =>   http://localhost:8080 `user: admin  password: changeme`

## Live app on: 

*  Flask API =>   http://silvio-cardoso.com/api/docs/  `apikey: changeme`
*  Kibana =>   http://silvio-cardoso.com:5601
*  rabbitmq => http://silvio-cardoso.com:15672 `user: admin  password: changeme`
*  phpMyAdmin =>   http://silvio-cardoso.com:8080 `user: admin  password: changeme`

# LIMITATIONS

*   This is for DEMO propose, almost of configurations from elastic, database, message broker must be properly configured for prod!

*   For the monitoring on elasticsearch the pattern generate in flask-app as well the grok pattern must be improved

*   For the API security, the hardcoded apikey ins't ready for production. An api gateway as https://aws.amazon.com/api-gateway/faqs/ should manage these keys.

*   Due the time limitation, i did not configure it. 
*   Through this demo is already possible to evaluate the Rest API, monitoring and search engine, and other tools needed for Data Engineering are working

### TESTS

1. Only integration tests, therefore the mysql should be running before to execute them
2. Maker sure your python has the necessary requirements on requirements.txt
3. RUN the command:  `python -m pytest .\flask_app\tests\ --cov=.\flask_app\tests\ -v` on in the root directory