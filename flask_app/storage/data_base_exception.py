class Data_base_Exception(Exception):
    
    def __init__(self, json, message="Error while interacting with Dtabase"):
        self.json = json
        self.message = message
        super().__init__(self.message)