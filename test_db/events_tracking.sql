
SET time_zone = "+01:00";

DROP DATABASE IF EXISTS events;
CREATE DATABASE IF NOT EXISTS events;
USE events;


DROP TABLE IF EXISTS users, 
                     events;

CREATE TABLE users (
    id                  INT             NOT NULL AUTO_INCREMENT,
    nickname            VARCHAR(100)     NOT NULL, 
    registration_date   DATE            NOT NULL,
    PRIMARY KEY (id)
) 
; 

CREATE TABLE events (
    id          INT             NOT NULL AUTO_INCREMENT,
    userId      INT             NOT NULL,
    event_type  VARCHAR(30)     NOT NULL,    
    event_datetime   DATETIME   NOT NULL,
    FOREIGN KEY (userId) REFERENCES users (id),
    PRIMARY KEY (id)
);

START TRANSACTION;

INSERT INTO `users` (`id`, `nickname`, `registration_date`) VALUES
(1, 'silvio', '2020-01-04'),
(2, 'dadazinha', '2020-01-04'),
(3, 'Annabelle', '2021-01-04'),
(4, 'Charlotte', '2021-01-04'),
(5, 'Chloe', '2018-01-04');

INSERT INTO `events` (`id`, `userId`, `event_type`, `event_datetime`) VALUES
(1, 2, 'email', '2021-01-04 00:00:00'),
(2, 2, 'newsletter', '2021-01-03 00:00:00'),
(3, 2, 'email to agency', '2021-01-02 00:00:00'),
(4, 2, 'consent', '2021-01-01 00:00:00'),
(5, 3, 'newsletter', '2017-07-21 16:32:28'),
(6, 3, 'newsletter', '2017-07-21 17:32:28'),
(7, 3, 'newsletter', '2017-07-21 18:32:28');

COMMIT;