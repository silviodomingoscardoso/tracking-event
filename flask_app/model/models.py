from dateutil import tz
from dateutil.parser import parse
import datetime
from marshmallow import Schema, fields, ValidationError

def checkDate(data):
    today = datetime.datetime.now(tz=tz.gettz("Europe/Paris")).date()
    if data > today:
        raise ValidationError("Date should be lower or equal to today.")

def checkDateTime(data):
    today = datetime.datetime.now(tz=tz.gettz("Europe/Paris"))
    try:
        data > today
    except Exception as err:
        raise ValidationError("Date time missing timezone.")

    if data > today:
        raise ValidationError("Event date should be lower or equal to today.")

def maxSize100(data):
    if  len(data)<5 or len(data)>100:
        raise ValidationError("nickname must have at least 5 characters and max of 100.")

def maxSize30(data):
    if  len(data)<3 or len(data)>30:
        raise ValidationError("event type must have at least 3 characters and max of 30.")

def check_Id(data):
    if data <= 0:
        raise ValidationError("The Id should be positive")

def check_userId(data):
    if data <= 0:
        raise ValidationError("The userId should be positive")


class UserSchema(Schema):
    id = fields.Int(dump_only=True)
    nickname = fields.Str(validate=maxSize100, required=True)
    registration_date = fields.Date(format="iso", validate=checkDate, required=True)

class EventSchema(Schema):
    id = fields.Int(dump_only=True)
    user = fields.Nested(UserSchema(), required=True)
    event_type = fields.Str(validate=maxSize30, required=True)
    event_datetime = fields.DateTime(format="iso", validate=checkDateTime, required=True)

class UserPostSchema(Schema):
    id = fields.Int(load_only=True)
    nickname = fields.Str(validate=maxSize100, required=True)
    registration_date = fields.Date(format="iso", validate=checkDate, required=True)

class EventPostSchema(Schema):
    id = fields.Int(load_only=True)
    userId = fields.Int(validate=check_userId, required=True)
    event_type = fields.Str(validate=maxSize30, required=True)
    event_datetime = fields.DateTime(format="iso", validate=checkDateTime, required=True)

class UserPutSchema(Schema):
    id = fields.Int(validate=check_Id)
    nickname = fields.Str(validate=maxSize100, required=True)
    registration_date = fields.Date(format="iso", validate=checkDate, required=True)

class ApiResponse(Schema): 
    code = fields.Int()
    _type = fields.Str()
    message	= fields.Str()