
import os
import logging
from os import environ
from flask import Flask
from celery import Celery
from celery.utils.log import get_task_logger
from flask import Flask
from kombu import Queue 
from dateutil.parser import parse  
from app.config_env import DevelopmentConfig
from app.model import db, Events

path = os.path.dirname(os.path.realpath(__file__))
logging.basicConfig(filename=os.path.join(path,'./logs/event_tracking.log'), level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
logging.handlers.TimedRotatingFileHandler(filename=os.path.join(path,'./logs/event_tracking.log'), when='d', interval=1)


logger = get_task_logger(__name__)


# https://flask.palletsprojects.com/en/1.1.x/patterns/celery/
def make_celery(app):
    celery = Celery(
        "events",
        broker=f"amqp://{environ.get('RABBITMQ_USERNAME')}:{environ.get('RABBITMQ_PASSWORD')}@{environ.get('RABBITMQ_URL')}/"
    )
    celery.conf.task_queues = (
        Queue('events', routing_key='tasks.store_event'),
    )
    celery.conf.task_default_exchange = 'events'
    celery.conf.task_default_exchange_type = 'direct'
    celery.conf.update(app.config)
    celery.conf.update(
        accept_content = ['json'],
        task_serializer = 'json',
        result_serializer = 'json', 
    )
    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


flask_app = Flask(__name__)
flask_app.config.from_object(DevelopmentConfig()) 
db.init_app(flask_app)
celery = make_celery(flask_app)


@celery.task(queue="events", bind=True)
def store_event(self, data):
     # Create new event
    event = Events(userId=data["userId"], event_type=data["event_type"],
                   event_datetime=parse(data["event_datetime"]))
    try:
        db.session.add(event)
        db.session.commit()
        logger.info(f"Event: {event.event_type} OK")
    except Exception as err:
        logger.error('failed to include event in database: %s', err)
        raise err