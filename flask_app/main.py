import os
from os import environ
from flask import jsonify, make_response, request, Flask, send_from_directory
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from model.models import ApiResponse
from storage.util_db import db 
from config_env import DevelopmentConfig
from routes import routes
import logging

path = os.path.dirname(os.path.realpath(__file__))
logging.basicConfig(filename=os.path.join(path,'./logs/event_tracking.log'), level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
logging.handlers.TimedRotatingFileHandler(filename=os.path.join(path,'./logs/event_tracking.log'), when='d', interval=1)

### swagger ui ###
SWAGGER_URL = '/api/docs'
API_URL = '/static/swagger.json'
SWAGGERUI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "atHome Technical evaluation"
    }
)

def create_app(config_module=None):
    app = Flask(__name__)
    app.config.from_object(DevelopmentConfig())
    app.register_blueprint(SWAGGERUI_BLUEPRINT, url_prefix=SWAGGER_URL)
    app.register_blueprint(routes.get_blueprint())
    app.logger.info("Flask App Ready To Go")
    db.set_logger(app.logger)
    CORS(app)
    db.get_db().init_app(app)
    return app


app = create_app()
api_resp = ApiResponse()

@app.before_request
def before_request():
    try:
        if "/v2/" in request.path and  request.headers.get("apiKey") != environ.get('apiKey') :
            raise Exception("forbidden API key not present")
    except Exception as err:
        return make_response(jsonify(api_resp.dump(
                {"code": 403, "_type": "error", "message": "forbidden API key not present"})), 403)



@app.route("/docs/swagger.json")
def specs():
    return send_from_directory("/static", "swagger.json")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
