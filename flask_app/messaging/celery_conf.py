from os import environ
from celery import Celery
from kombu import Queue

celery = Celery(
        "events",
        broker=f"amqp://{environ.get('RABBITMQ_USERNAME')}:{environ.get('RABBITMQ_PASSWORD')}@{environ.get('RABBITMQ_URL')}/"
    )
celery.conf.task_queues = (
        Queue('events', routing_key='tasks.store_event'),
    )
celery.conf.task_default_exchange = 'events'
celery.conf.task_default_exchange_type = 'direct'