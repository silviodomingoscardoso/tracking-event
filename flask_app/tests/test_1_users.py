import requests
import pytest
import json
from os import environ

BASE_URL = environ.get("BASE_URL", "http://localhost/v2")


def test_create_user_201():
    url = f"{BASE_URL}/user"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"nickname\": \"Camille\",\r\n    \"registration_date\": \"2019-01-04\"\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 201


def test_create_user_400_missing_date():

    url = f"{BASE_URL}/user"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"nickname\": \"Camille\" \r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_create_user_400_date_future():

    url = f"{BASE_URL}/user"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"nickname\": \"Camille\",\r\n    \"registration_date\": \"3019-01-04\"\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_create_user_400_missing_nickname():

    url = f"{BASE_URL}/user"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"registration_date\": \"2019-01-04\"\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_create_user_403():

    url = f"{BASE_URL}/user"
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"nickname\": \"Camille\",\r\n    \"registration_date\": \"2019-01-04\"\r\n}"
    response = requests.request("POST", url, headers=headers, data=payload)
    assert response.status_code == 403


def test_update_user_200():

    url = f"{BASE_URL}/user/3"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"nickname\": \"Camille updated\",\r\n    \"registration_date\": \"2020-01-04\"\r\n}"
    response = requests.request("PUT", url, headers=headers, data=payload)
    assert response.status_code == 200


def test_update_user_400_missing_nickname():

    url = f"{BASE_URL}/user/3"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n   \"registration_date\": \"2019-01-04\" \r\n}"
    response = requests.request("PUT", url, headers=headers, data=payload)
    print(response.json())
    assert response.status_code == 400


def test_update_user_400_missing_date():

    url = f"{BASE_URL}/user/3"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"nickname\": \"Camille\"\r\n}"
    response = requests.request("PUT", url, headers=headers, data=payload)
    assert response.status_code == 400


def test_update_user_403():

    url = f"{BASE_URL}/user/3"
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    payload = "{\r\n    \"nickname\": \"Camille\",\r\n    \"registration_date\": \"2019-01-04\"\r\n}"
    response = requests.request("PUT", url, headers=headers, data=payload)
    assert response.status_code == 403


def test_get_user_200():

    url = f"{BASE_URL}/user"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    response = requests.request("GET", url, headers=headers)
    assert response.status_code == 200
    assert len(response.json()) > 1


def test_get_user_200_nickname():

    url = f"{BASE_URL}/user?nickname=silvio"
    headers = {
        'Accept': 'application/json',
        'apiKey': 'changeme',
        'Content-Type': 'application/json'
    }
    response = requests.request("GET", url, headers=headers)
    assert response.status_code == 200


def test_get_user_403():

    url = f"{BASE_URL}/user?nickname=silvio"
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    response = requests.request("GET", url, headers=headers)
    assert response.status_code == 403
